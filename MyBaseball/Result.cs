﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBaseball
{
    class Result
    {
        public int Strike;
        public int Ball;
        public int Out;

        public bool IsCorrect()
        {
            // 엄청 복잡한 로직이라능
            return Strike == Constant.Digit;
        }

        public void Print()
        {
            Console.WriteLine($"S: {Strike}, B: {Ball}, O: {Out}");
        }

        public void Calculate(int[] answers, int[] guesses)
        {
            for (int i = 0; i < Constant.Digit; i++)
            {
                for (int j = 0; j < Constant.Digit; j++)
                {
                    if (i == j)
                    {
                        if (answers[i] == guesses[i])
                        {
                            Strike++;
                            break;
                        }
                    }
                    else // i != j
                    {
                        if (answers[i] == guesses[j])
                        {
                            Ball++;
                            break;
                        }
                    }
                    if (j == Constant.Digit - 1)
                        Out++;
                }
            }
        }
    }
}
