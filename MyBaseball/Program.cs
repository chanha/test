﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBaseball
{
    class Program
    {

        static int[] CreateAnswers()
        {
            Random random = new Random();
            int[] answers = new int[Constant.Digit];

            while (true)
            {
                for (int i = 0; i < Constant.Digit; i++)
                    answers[i] = random.Next(Constant.MaxValue);
                
                // todo: 아마도 수정 완료
                for (int i = 0; i < Constant.Digit; i++)
                {
                    for (int j = 0; j < Constant.Digit; j++)
                    {
                        if (i != j)
                        {
                            if (answers[i] == answers[j])
                                continue;
                        }
                    }
                }
                break;
            }
            return answers;
        }

        static void PrintNumbers(string prefix, int[] Numbers)
        {
            Console.WriteLine(prefix);
            for (int i = 0; i < Constant.Digit; i++)
                Console.Write(Numbers[i] + " ");
            Console.WriteLine();
        }

        static int[] InputGuesses()
        {
            int[] guesses = new int[Constant.Digit];
            for (int i = 0; i < guesses.Length; i++) // property
                guesses[i] = int.Parse(Console.ReadLine());
            return guesses;
        }

        static void Main(string[] args)
        {
            // 1. (중복되지 않는 세 개의 0~9 사이의 정수로 이루어진)
            // 정답을 생성한다.
            int[] answers = CreateAnswers();

            PrintNumbers("[정답] ", answers);

            int tryCount = 0;

            while (true)
            {
                tryCount++;

                // 2. 추측을 입력받는다.
                int[] guesses = InputGuesses();

                PrintNumbers("[추측] ", answers);

                // 3. 정답과 추측을 비교하여 결과를 생성한다.
                Result result = new Result();

                result.Calculate(answers, guesses);

                // 4. 결과를 출력한다.
                result.Print();

                // 5. 정답과 추측이 일치하지 않으면 2번으로 돌아간다.
                if (result.IsCorrect())
                    break;
            }

            // 6. 정답을 맞추는데 걸린 횟수를 출력하고 종료한다.
            Console.WriteLine($"총 {tryCount}번 만에 맞추었습니다");
        }
    }
}
